# todo-list-react


| **开发人员** | [@MangoRu](https://gitea.com/MangoRu) |
| ------------ | -------------------------------- |
| **技术栈**   | React + TS           |


## 项目介绍
![img.png](readme%2Fimg.png)

该项目利用 React + TS 简单实现一个 TODO 任务列表案例（使用React 类组件方式）。此项目包含以下功能：

1. 展示任务列表：列出所有任务及状态信息。
2. 添加任务：允许用户输入新任务信息并添加到列表中。
3. 删除任务：从列表中移除任务。
4. 清空任务：清空所有任务。

## 项目命令

### 创建项目

CRA 创建支持 TS 的 React 项目：
```shell
npx create-react-app todo-list-react --template typescript
```
### 项目启动
```shell
npm start
```

<br>

---

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

### Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
