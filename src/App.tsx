import React, {Component} from 'react'
import './App.css'
import './css/index.css'

import TodoAdd from "./components/TodoAdd"
import TodoList from "./components/TodoList"
import TodoFooter from "./components/TodoFooter"

/*
* 展示任务列表功能
* 1.为父组件App组件 提供状态（任务列表数据）和类型
* 2.为子组件TodoList组件 指定接收到的 props类型
* 3.将任务列表数据传递给子组件TodoList组件
*/
import {TodoItem} from "./type/todos";

type Todos = {
    todos: TodoItem[]
    num: number
}
const todoList: TodoItem[] = [
    {id: 1, text: 'Learn React', done: false},
    {id: 2, text: 'Learn Redux', done: false},
    {id: 3, text: 'Learn TypeScript', done: true}
]

class App extends Component<{}, Todos> {
    state: Todos = {
        todos: todoList,
        num: todoList.length
    }
    addTask = (task: string) => {
        this.setState({
            todos: [
                ...this.state.todos,
                {
                    id: this.state.todos.length + 1,
                    text: task,
                    done: false
                }
            ]
        })
    }
    clearTaskAll = () => {
        if(!!this.state.todos.length) {
            this.setState({
                todos: [],
                num: 0
            })
        }
    }
    deleteTask = (id: number) => {
        this.state.todos.splice(id, 1)
        this.setState({
            todos: this.state.todos,
            num: this.state.todos.length - 1
        })
    }

    render() {
        return (
            <section id="app">
                <TodoAdd onAdd={this.addTask}></TodoAdd>
                <TodoList list={this.state.todos} onDelete={this.deleteTask}></TodoList>
                <TodoFooter onClear={this.clearTaskAll} num={this.state.num}></TodoFooter>
            </section>
        )
    }
}

export default App
