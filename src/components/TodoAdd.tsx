import React from "react";

/*
* 添加任务功能
* 思路：子组件获取文本框值，父子组件传值（子传父），父组件改变状态列表
* 1.子组件添加状态和属性：状态-文本框的值，属性-回调函数，接受一个string类型参数
* 2.通过受控组件方式获取文本框的值
* 3.子组件文本框按下回车键传递数据给父组件
* 4.父组件接受子组件数据（任务名称 <- 文本框的值）
* 5.任务添加到父组件状态数据中
* */
// 属性的类型
type TodoAddProps = {
    onAdd(task: string): void
}
// 状态的类型
type TodoAddState = {
    task: string
}

class TodoAdd extends React.Component<TodoAddProps, TodoAddState> {
    state: TodoAddState = {
        task: ''
    }
    onChangeValue = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            task: e.target.value
        })
    }
    onAddTask = (e: React.KeyboardEvent<HTMLInputElement>) => {
        // 获取文本框的值
        const {task} = this.state
        if (e.key === 'Enter' && !!task.trim()) {
            // 添加任务
            this.props.onAdd(task)
            // 清空文本框
            this.setState({
                task: ''
            })
        }
    }

    render() {
        return (
            // 输入框
            <header className="header">
                <h1>TODO 案例</h1>
                <input
                    placeholder="请输入任务"
                    className="new-todo"
                    autoFocus
                    value={this.state.task}
                    onChange={this.onChangeValue}
                    onKeyDown={this.onAddTask}
                />
                <button className="add">添加任务</button>
            </header>
        );
    }
}

export default TodoAdd;