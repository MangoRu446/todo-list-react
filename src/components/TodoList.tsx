import React from "react"
import {TodoItem} from "../type/todos"

interface TodoListProps {
    list: TodoItem[],
    onDelete(id: number): void
}
class TodoList extends React.Component<TodoListProps> {
    onclickDelete = (id: number) => () => {
        // 删除任务
        this.props.onDelete(id)
    }
    render()
    {
        return (
            // 列表
            <section className="main">
                <ul className="todo-list">
                    {/*编辑样式 editing 已完成样式 completed*/}
                    {/*列表渲染*/}
                    {
                        this.props.list.map((todo,index) => {
                            return (
                                <li className={todo.done ? 'completed' : ''} key={todo.id}>
                                    <div className="view">
                                        <span className="index">{index+1}.</span> <label>{todo.text}</label>
                                        <button className="destroy" onClick={this.onclickDelete(todo.id)}></button>
                                    </div>
                                </li>
                            )
                        })
                    }
                </ul>
            </section>
        );
    }
}

export default TodoList;