import React from "react";
// 属性的类型
type Props = {
    onClear(): void
    num: number
}
type State = {
    count: number
}
class TodoFooter extends React.Component<Props> {
    onClear = () => {
        this.props.onClear()
    }
    render()
    {
        const { num} = this.props
        return (
            // 统计和清空
            <footer className="footer">
                {/*统计*/}
                <span className="todo-count">合 计:<strong> {num} </strong> 条事项</span>
                {/*清空*/}
                <button className="clear-completed" onClick={this.onClear}>
                    清空任务
                </button>
            </footer>
        );
    }
}

export default TodoFooter;